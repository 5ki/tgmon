#!/bin/bash

PATH=".:$PATH"

CONDITION=$(df | grep [6-9].%)

if [[ $CONDITION ]]; then
	MESSAGE=$'===\n'
	MESSAGE=$MESSAGE"$(hostname)"
	MESSAGE=$MESSAGE$'\n---\n'
	MESSAGE=$MESSAGE"$(df -h | grep -v tmpfs)"
	tg.sh "$MESSAGE" `group.sh all.grp`
fi

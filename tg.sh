#!/bin/bash

LOGFILE="/tmp/tgmon.log"

MESSAGE="$1"
shift
CHAT_IDS="$@"

TOKEN=$(cat .token)

for CHAT_ID in $CHAT_IDS; do
	printf "\n[$(date -u +'%F %T %Z')] " >> $LOGFILE
	curl -sG \
		--data-urlencode "chat_id=$CHAT_ID" \
		--data-urlencode "text=$MESSAGE" \
		"https://api.telegram.org/bot$TOKEN/sendMessage" >> $LOGFILE
done

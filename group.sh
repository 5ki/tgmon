#!/bin/bash

grep -v "#" $@ | sort | uniq | awk 'BEGIN { ORS = " " } {print $2}'

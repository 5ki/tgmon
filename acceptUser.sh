#!/bin/bash

NAME="$1"
TOKEN=`cat .token`

CODE=`grep "$NAME" all.grp | cut -f 2`
#echo $CODE
BUF=`curl -sG https://api.telegram.org/bot$TOKEN/getUpdates`
#echo $BUF 
CHAT_ID=`jq '.result | map(select(.message.text == "'$CODE'")) | .[0].message.chat.id' <<< $BUF`
#echo $CHAT_ID

echo -e "$NAME\t$CHAT_ID" >> all.grp
cp all.grp .all.grp.bak
fgrep -v "#$NAME" .all.grp.bak > all.grp
